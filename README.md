# Task 2
###### Create the program for calculation fibonacci values.
###### To complete this task were used:
* java 8
* maven 3
* pmd
* checkstyle
* findBugs
###### To generate report please enter the command:
* mvn pmd:check  - for PMD
* mvn checkstyle:checkstyle  - for checkStyle
* mvn clean install - findbugs and PMD


