package epam.task;

import epam.task.calculations.Operation;

import java.util.*;


public class Main {

    public static void main(String[] args) {
        Operation operation = new Operation();
        Main main = new Main();
        List<Integer> params = main.getParameters();
        List<Integer> values = operation.setValues(params.get(0), params.get(1));
        main.printOddAndEvenValues(values);
        System.out.println("Sum of even element: " + operation.calculateSum(true, values));
        System.out.println("Sum of odd element: " + operation.calculateSum(false, values));
        main.printMaxOddAndEvenNumbers(params.get(2));
    }

    /**
     * Collects params for interval and fib sequence number value
     *
     * @return Integer List
     */
    private List<Integer> getParameters() {
        List<Integer> params = new ArrayList<>();
        params.add(getingParameter("Please enter the interval start value: "));
        params.add(getingParameter("Please enter the interval end value: "));
        params.add(getingParameter("Please enter the Fibonacci number: "));
        return params;
    }

    /**
     * Catch exception
     *
     * @param message print information for user
     * @return int value
     */
    private int getingParameter(String message) {
        int value = -1;
        do {
            Scanner in = new Scanner(System.in);
            System.out.println(message);
            try {
                value = in.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Exception. Expected integer input value");
            }
        } while (value <= 0);
        return value;
    }

    /**
     * Print odd and even values
     *
     * @param values Integer List
     */
    private void printOddAndEvenValues(List<Integer> values) {
        Operation operation = new Operation();
        List<Integer> valueListEven = operation.getValues(true, values);
        System.out.println("Even is: " + Arrays.toString(valueListEven.toArray()));
        List<Integer> valueListOdd = operation.getValues(false, values);
        Collections.reverse(valueListOdd);
        System.out.println("Odd is: " + Arrays.toString(valueListOdd.toArray()));
    }

    /**
     * Print max even and odd elements
     *
     * @param numElements Integer value
     */
    private void printMaxOddAndEvenNumbers(int numElements) {
        Operation operation = new Operation();
        System.out.println("F1 = " + operation.getMaxElement(true, operation.getFibonacciSequence(numElements)));
        System.out.println("F2 = " + operation.getMaxElement(false, operation.getFibonacciSequence(numElements)));
    }
}
