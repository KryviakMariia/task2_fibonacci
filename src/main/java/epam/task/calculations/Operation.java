package epam.task.calculations;

import java.util.ArrayList;
import java.util.List;

public class Operation {

    /**
     * Create list with odd or even Integer values from given list
     * @param isEven flag for choosing odd or even value
     * @param value Integer List
     * @return Integer List with odd or even value
     */
    public List<Integer> getValues(boolean isEven, List<Integer> value) {
        List<Integer> oddOrEven = new ArrayList<>();
        for (Integer integer : value) {
            if ((integer % 2 == 0) && isEven) {
                oddOrEven.add(integer);
            } else if ((integer % 2 == 1) && !isEven) {
                oddOrEven.add(integer);
            }
        }
        return oddOrEven;
    }

    /**
     * Calculate sum odd or even value
     * @param isEven flag for choosing odd or even value
     * @param oddOrEven Integer List
     * @return sum of odd or even element
     */
    public int calculateSum(boolean isEven, List<Integer> oddOrEven) {
        int sum = 0;
        for (Integer integer : oddOrEven) {
            if ((integer % 2 == 0) && isEven) {
                sum += integer;
            } else if ((integer % 2 == 1) && !isEven) {
                sum += integer;
            }
        }
        return sum;
    }

    /**
     * Generates Fibonacci Sequence by given element
     * @param numOfElement Integer List
     * @return Integer List with Fibonacci value
     */
    public List<Integer> getFibonacciSequence(int numOfElement) {
        List<Integer> fibNumbers = new ArrayList<>();
        for (int i = 0; i < numOfElement; i++) {
            fibNumbers.add(fib(i));
        }
        return fibNumbers;
    }

    /**
     * Calculate Fibonacci value
     * @param n integer value
     * @return Fibonacci value
     */
    private int fib(int n) {
        if (n <= 1)
            return n;
        return fib(n - 1) + fib(n - 2);
    }

    /**
     * Selects the biggest odd or even value from given list
     * @param isEven flag for choosing odd or even value
     * @param valuesList Integer List
     * @return odd or even maximize value
     */
    public int getMaxElement(boolean isEven, List<Integer> valuesList) {
        int max = 0;
        for (Integer integer : valuesList) {
            if (integer > max) {
                if ((integer % 2) == 0 && isEven) {
                    max = integer;
                } else if ((integer % 2) == 1 && !isEven) {
                    max = integer;
                }
            }
        }
        return max;
    }

    /**
     * Generates filled list by interval
     * @param start first interval value
     * @param end last interval value
     * @return Integer List
     */
    public List<Integer> setValues(int start, int end) {
        List<Integer> value = new ArrayList<>();
        for (int i = start; i < end; i++) {
            value.add(i);
        }
        return value;
    }
}
